# Mirror of Sencha Ext JS GPLv3

This repository is a mirror; the original Ext JS 7.0.0 GPLv3 bundle is available from the [official site](https://www.sencha.com/legal/gpl/).

## Modifications

- `LICENSE` file with a copy of GPLv3 was introduced while the original `license.txt` file was renamed to `license-orig.txt` to avoid displaying incorrect license in GitLab license indicator.
- `build` was omitted to greatly reduce space requirements.

# Sencha Ext JS

This is the Sencha Ext JS Framework Package, or just "ext" for short.

## Installation

Install [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install --save @extjs/ext
```

## Dependencies

This will need [@extjs/ext-build](https://github.com/sencha/extjs-reactor/tree/2.0.x-dev/packages/ext-build) to work.

This will be included if you generate the application using [@extjs/ext-gen](https://github.com/sencha/extjs-reactor/tree/2.0.x-dev/packages/ext-gen)

